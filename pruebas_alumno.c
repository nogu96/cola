#include "cola.h"
#include "testing.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h> 

void** crear_valores(size_t tamanio) {
	void** arr = malloc(sizeof(void*) * tamanio);
	if(arr == NULL)
		return NULL;
	for(int i=0; i< tamanio; i++) {
		arr[i] = malloc(sizeof(void*));
		if(arr[i] == NULL) {
			for (int j=0; j<i; i++)
				free(arr[j]);
			free(arr);
			return NULL;
		}
	}

	return arr;
}

void desencolar_muchos_elementos() {
	printf("------------DESENCOLAR MUCHOS ELEMENTOS-------------\n");
	size_t VECTOR_SIZE = 1000;
	cola_t* cola = cola_crear();
	void** vector = crear_valores(VECTOR_SIZE);

	for(int i=0; i<VECTOR_SIZE; i++) {
		cola_encolar(cola, vector[i]);
	}
	for(int i=0; i<VECTOR_SIZE; i++) {
		printf("elemento "); printf("%p", vector[i]); printf(" desencolado: ");
		print_test("", cola_desencolar(cola) == vector[i]);
		free(vector[i]);
	}

	print_test("Cola esta vacia", cola_esta_vacia(cola));

	cola_destruir(cola, NULL);
	free(vector);
}

void encolar_null() {
	printf("------------ENCOLAR NULL-------------\n");
	cola_t* cola = cola_crear();
	print_test("desencolar con cola vacia", cola_desencolar(cola) == NULL);
	
	char* string = NULL;
	cola_encolar(cola, NULL);
	cola_encolar(cola, &string);

	print_test("NULL desencolado", cola_desencolar(cola) == NULL);
	print_test("String NULL desencolado", cola_desencolar(cola) == &string);

	print_test("Cola esta vacia", cola_esta_vacia(cola));

	cola_destruir(cola, NULL);
}

void cola_vacia() {
	printf("------------COLA VACIA-------------\n");
	cola_t* cola = cola_crear();

	print_test("Cola esta vacia", cola_esta_vacia(cola));
	print_test("Desapilar cola vacia = NULL", cola_desencolar(cola) == NULL);
	print_test("Ver primero cola = NULL", cola_ver_primero(cola) == NULL);

	size_t valor = 5;
	cola_encolar(cola, &valor);
	cola_desencolar(cola);
	printf("Valor encolado y desencolado:\n");
	
	print_test("Cola esta vacia", cola_esta_vacia(cola));
	print_test("Desapilar cola vacia = NULL", cola_desencolar(cola) == NULL);
	print_test("Ver primero cola = NULL", cola_ver_primero(cola) == NULL);

	cola_destruir(cola, NULL);
}

void pruebas_cola_alumno() {
	desencolar_muchos_elementos();
	encolar_null();
	cola_vacia();
}